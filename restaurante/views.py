from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from .models import Client, Waiter, Table, Product, Invoice, Order
from .forms import ClientForm, WaiterForm, TableForm, ProductForm, InvoiceForm, OrderForm
from django.urls import reverse_lazy
from rest_framework.views import APIView
from restaurante.serializers import ClientSerializer, WaiterSerializer, TableSerializer, ProductSerializer, InvoiceSerializer, OrderSerializer
from rest_framework import permissions
from rest_framework import viewsets

def index(request):
    return render(request, 'home.html')

class ClientListView(ListView):
    model = Client
    template_name = 'client/client_list.html'
    
class ClientDetailView(DetailView):
    model = Client
    template_name = 'client/client_detail.html'
    ordering = 'id'
    context_object_name = 'client'

class ClientCreateView(CreateView):
    model = Client
    template_name = 'client/create_client.html'
    form_class = ClientForm
    success_url = reverse_lazy('client_list')

class ClientUpdateView(UpdateView):
    model = Client
    template_name = 'client/create_client.html'
    fields = ['first_name', 'last_name', 'observations']
    success_url = reverse_lazy('client_list')

class ClientDeleteView(DeleteView):
    model = Client
    template_name = 'client/client_delete.html'
    success_url = reverse_lazy('client_list')


class WaiterListView(ListView):
    model = Waiter
    template_name = 'waiter/waiter_list.html'

class WaiterDetailView(DetailView):
    model = Waiter
    template_name = 'waiter/waiter_detail.html'
    ordering = 'id'
    context_object_name = 'waiter'

class WaiterCreateView(CreateView):
    model = Waiter
    template_name = 'waiter/create_waiter.html'
    form_class = WaiterForm
    success_url = reverse_lazy('waiter_list')

class WaiterUpdateView(UpdateView):
    model = Waiter
    template_name = 'waiter/create_waiter.html'
    fields = ['first_name', 'last_name']
    success_url = reverse_lazy('waiter_list')

class WaiterDeleteView(DeleteView):
    model = Waiter
    template_name = 'waiter/waiter_delete.html'
    success_url = reverse_lazy('waiter_list')


class TableListView(ListView):
    model = Table
    template_name = 'table/table_list.html'

class TableDetailView(DetailView):
    model = Table
    template_name = 'table/table_detail.html'
    ordering = 'id'
    context_object_name = 'table'

class TableCreateView(CreateView):
    model = Table
    template_name = 'table/create_table.html'
    form_class = TableForm
    success_url = reverse_lazy('table_list')

class TableUpdateView(UpdateView):
    model = Table
    template_name = 'table/create_table.html'
    fields = ['num_diner', 'location']
    success_url = reverse_lazy('table_list')

class TableDeleteView(DeleteView):
    model = Table
    template_name = 'table/table_delete.html'
    success_url = reverse_lazy('table_list')

class ProductListView(ListView):
    model = Product
    template_name = 'product/product_list.html'

class ProductDetailView(DetailView):
    model = Product
    template_name = 'product/product_detail.html'
    ordering = 'id'
    context_object_name = 'product'

class ProductCreateView(CreateView):
    model = Product
    template_name = 'product/create_product.html'
    form_class = ProductForm
    success_url = reverse_lazy('product_list')

class ProductUpdateView(UpdateView):
    model = Product
    template_name = 'product/create_product.html'
    fields = ['name', 'description', 'imported']
    success_url = reverse_lazy('product_list')

class ProductDeleteView(DeleteView):
    model = Product
    template_name = 'product/product_delete.html'
    success_url = reverse_lazy('product_list')

class InvoiceListView(ListView):
    model = Invoice
    template_name = 'invoice/invoice_list.html'

    
class InvoiceDetailView(DetailView):
    model = Invoice
    template_name = 'invoice/invoice_detail.html'

class InvoiceCreateView(CreateView):
    model = Invoice
    template_name = 'invoice/create_invoice.html'
    form_class = InvoiceForm
    success_url = reverse_lazy('invoice_list')

class InvoiceUpdateView(UpdateView):
    model = Invoice
    template_name = 'invoice/create_invoice.html'
    fields = ['date_invoice','id_client','id_waiter','id_table']
    success_url = reverse_lazy('invoice_list')

class InvoiceDeleteView(DeleteView):
    model = Invoice
    template_name = 'invoice/invoice_delete.html'
    success_url = reverse_lazy('invoice_list')

class OrderListView(ListView):
    model = Order
    template_name = 'order/order_list.html'

class OrderDetailView(DetailView):
    model = Order
    template_name = 'order/order_detail.html'
    ordering = 'id'
    context_object_name = 'order'

class OrderCreateView(CreateView):
    model = Order
    template_name = 'order/create_order.html'
    form_class = OrderForm
    success_url = reverse_lazy('order_list')

class OrderUpdateView(UpdateView):
    model = Order
    template_name = 'order/create_order.html'
    fields = ['quantity','products','invoices']
    success_url = reverse_lazy('order_list')

class OrderDeleteView(DeleteView):
    model = Order
    template_name = 'order/order_delete.html'
    success_url = reverse_lazy('order_list')


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    #permission_classes = [permissions.IsAuthenticated]


class WaiterViewSet(viewsets.ModelViewSet):
    queryset = Waiter.objects.all()
    serializer_class = WaiterSerializer
    

class TableViewSet(viewsets.ModelViewSet):
    queryset = Table.objects.all()
    serializer_class = TableSerializer
    

class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

class InvoiceViewSet(viewsets.ModelViewSet):
    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer

class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer


    

