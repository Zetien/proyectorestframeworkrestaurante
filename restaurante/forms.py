from django import forms
from restaurante.models import Client, Waiter, Table, Product, Invoice, Order

class ClientForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = ('first_name', 'last_name', 'observations')

class WaiterForm(forms.ModelForm):
    class Meta:
        model = Waiter
        fields = ('first_name', 'last_name')

class TableForm(forms.ModelForm):
    class Meta:
        model = Table
        fields = ('num_diner', 'location')

class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ('name', 'description', 'imported')

class InvoiceForm(forms.ModelForm):
    date_invoice = forms.DateField(widget=forms.SelectDateWidget )
    id_client = forms.ModelChoiceField(queryset=Client.objects.all())
    id_waiter = forms.ModelChoiceField(queryset=Waiter.objects.all())
    id_table = forms.ModelChoiceField(queryset=Table.objects.all())
    class Meta:
        model = Invoice
        fields = ('date_invoice', 'id_client', 'id_waiter', 'id_table')

class OrderForm(forms.ModelForm):
    quantity = forms.IntegerField()
    products = forms.ModelChoiceField(queryset=Product.objects.all())
    invoices = forms.ModelChoiceField(queryset=Invoice.objects.all())
    class Meta:
        model = Order
        fields = ('quantity', 'products', 'invoices')
