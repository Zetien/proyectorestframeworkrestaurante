# PROJECT RESTAURANT REST FRAMEWORK

>## **Made by**
- Jorge Luis Zetien Luna



> ### **How to run the project**
You need a virtual environment to run the project, like virtualenv, then just run the requirements.txt
to install the items to run this project


> ### **Clone repo link**
`https://gitlab.com/Zetien/proyectorestframeworkrestaurante.git`



> ### **Install items files to run this project**
`pip install -r requirements.txt`


> ### **Compile project**
`python manage.py makemigrations`
`python manage.py migrate`
`python manage.py runserver`


> ### **Requeriments of this project**
|            Key               |       value           |
|------------------------------|-----------------------|
|django                        |       4.0.4           |
|djangorestframework           |       3.13.1          |
|djangorestframework-simplejwt |       13.6.0          |
|PyJWT                         |       2.3.0           |   
|pytz                          |       2022.1          |
|sqlparse                      |       0.4.2           |
|asgiref                       |       3.5.1           |



