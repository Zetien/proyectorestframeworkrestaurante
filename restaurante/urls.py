from django.urls import path
from rest_framework import routers
from restaurante.views import *


router = routers.DefaultRouter()
router.register(r'client', ClientViewSet)
router.register(r'waiter', WaiterViewSet)
router.register(r'table', TableViewSet)
router.register(r'product', ProductViewSet)
router.register(r'invoice', InvoiceViewSet)
router.register(r'order', OrderViewSet)



urlpatterns = [
    path('', index, name='index'),
    path('client_list/', ClientListView.as_view(), name='client_list'),
    path('client_detail/<int:pk>/', ClientDetailView.as_view(), name='client_detail'),
    path('client_create/', ClientCreateView.as_view(), name='client_create'),
    path('client_update/<int:pk>/', ClientUpdateView.as_view(), name='client_update'),
    path('client_delete/<int:pk>/', ClientDeleteView.as_view(), name='client_delete'),
    path('waiter_list/', WaiterListView.as_view(), name='waiter_list'),
    path('waiter_detail/<int:pk>/', WaiterDetailView.as_view(), name='waiter_detail'),
    path('waiter_create/', WaiterCreateView.as_view(), name='waiter_create'),
    path('waiter_update/<int:pk>/', WaiterUpdateView.as_view(), name='waiter_update'),
    path('waiter_delete/<int:pk>/', WaiterDeleteView.as_view(), name='waiter_delete'),
    path('table_list/', TableListView.as_view(), name='table_list'),
    path('table_detail/<int:pk>/', TableDetailView.as_view(), name='table_detail'),
    path('table_create/', TableCreateView.as_view(), name='table_create'),
    path('table_update/<int:pk>/', TableUpdateView.as_view(), name='table_update'),
    path('table_delete/<int:pk>/', TableDeleteView.as_view(), name='table_delete'),
    path('product_list/', ProductListView.as_view(), name='product_list'),
    path('product_detail/<int:pk>/', ProductDetailView.as_view(), name='product_detail'),
    path('product_create/', ProductCreateView.as_view(), name='product_create'),
    path('product_update/<int:pk>/', ProductUpdateView.as_view(), name='product_update'),
    path('product_delete/<int:pk>/', ProductDeleteView.as_view(), name='product_delete'),
    path('invoice_list/', InvoiceListView.as_view(), name='invoice_list'),
    path('invoice_detail/<int:pk>/', InvoiceDetailView.as_view(), name='invoice_detail'),
    path('invoice_create/', InvoiceCreateView.as_view(), name='invoice_create'),
    path('invoice_update/<int:pk>/', InvoiceUpdateView.as_view(), name='invoice_update'),
    path('invoice_delete/<int:pk>/', InvoiceDeleteView.as_view(), name='invoice_delete'),
    path('order_list/', OrderListView.as_view(), name='order_list'),
    path('order_detail/<int:pk>/', OrderDetailView.as_view(), name='order_detail'),
    path('order_create/', OrderCreateView.as_view(), name='order_create'),
    path('order_update/<int:pk>/', OrderUpdateView.as_view(), name='order_update'),
    path('order_delete/<int:pk>/', OrderDeleteView.as_view(), name='order_delete'),




    
]